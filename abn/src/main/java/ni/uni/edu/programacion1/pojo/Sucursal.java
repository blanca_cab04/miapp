/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.uni.edu.programacion1.pojo;

/**
 *
 * @author Blanca Payan
 */
public class Sucursal {
    int id; //4bytes
    String nombreSucursal; //30 caracteres * 2 bytes = 60 bytes + 3 UTF = 63 bytes 
    Municipio m; //130 bytes
    String direccion; //75 caracteres * 2 bytes = 150 + 3 bytes UTF = 153 bytes
    String telefono; //9 caracteres * 2 bytes = 18 + 3 bytes UTF = 21 bytes
    String correo; //25 caracteres * 2 bytes = 50 + 3 bytes UTF = 53 bytes
    int activo; //0-inactivo, 1-activo //4 bytes
    String fechaCreacion; //10 caracteres * 2 bytes = 20 + 3 bytes UTF = 23
    String fechaModificado; //10 caracteres * 2 bytes = 20 + 3 bytes UTF = 23
    
    /*
        PESO TOTAL SUCURSAL = 474 BYTES
    */

    public Sucursal() {
    }

    public Sucursal(int id, String nombreSucursal, Municipio m, String direccion, String telefono, String correo, int activo, String fechaCreacion, String fechaModificado) {
        this.id = id;
        this.nombreSucursal = nombreSucursal;
        this.m = m;
        this.direccion = direccion;
        this.telefono = telefono;
        this.correo = correo;
        this.activo = activo;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificado = fechaModificado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreSucursal() {
        return nombreSucursal;
    }

    public void setNombreSucursal(String nombreSucursal) {
        this.nombreSucursal = nombreSucursal;
    }

    public Municipio getM() {
        return m;
    }

    public void setM(Municipio m) {
        this.m = m;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getFechaModificado() {
        return fechaModificado;
    }

    public void setFechaModificado(String fechaModificado) {
        this.fechaModificado = fechaModificado;
    }

    @Override
    public String toString() {
        return "Sucursal{" + "id=" + id + ", nombreSucursal=" + nombreSucursal + ", municipio=" + m.getNombreMunicipio() + ", direccion=" + direccion + ", telefono=" + telefono + ", correo=" + correo + ", activo=" + activo + ", fechaCreacion=" + fechaCreacion + ", fechaModificado=" + fechaModificado + '}';
    }
    
    
}
