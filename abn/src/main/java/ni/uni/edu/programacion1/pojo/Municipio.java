/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.uni.edu.programacion1.pojo;

/**
 *
 * @author Blanca Payan
 */
public class Municipio {
    int idMunicipio;//4bytes
    String nombreMunicipio; //20 caracteres * 2bytes = 40 bytes + 3bytes UTF = 43 bytes
    String nombreDepartamento; //40 caracteres * 2bytes = 80 bytes + 3bytes UTF = 83 bytes
    
    /*
        PESO TOTAL MUNICIPIO = 130 BYTES
    */

    public Municipio() {
    }

    public Municipio(int idMunicipio, String nombreMunicipio, String nombreDepartamento) {
        this.idMunicipio = idMunicipio;
        this.nombreMunicipio = nombreMunicipio;
        this.nombreDepartamento = nombreDepartamento;
    }

    public int getIdMunicipio() {
        return idMunicipio;
    }

    public void setIdMunicipio(int idMunicipio) {
        this.idMunicipio = idMunicipio;
    }

    public String getNombreMunicipio() {
        return nombreMunicipio;
    }

    public void setNombreMunicipio(String nombreMunicipio) {
        this.nombreMunicipio = nombreMunicipio;
    }

    public String getNombreDepartamento() {
        return nombreDepartamento;
    }

    public void setNombreDepartamento(String nombreDepartamento) {
        this.nombreDepartamento = nombreDepartamento;
    }
    
    
}
