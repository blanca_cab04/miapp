/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.uni.edu.programacion1.daoImplement;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import ni.uni.edu.programacion1.dao.SucursalInterface;
import ni.uni.edu.programacion1.pojo.Sucursal;

/**
 *
 * @author Blanca Payan
 */
public class SucursalImplement implements SucursalInterface<Sucursal>{
    
    final int TAMAÑO = 474; //SIZE
    File archivo; //file 
    String urlArchivo; //path
    RandomAccessFile flujo; //raf

    public SucursalImplement() {
        urlArchivo = "sucursales.dat";
        archivo = new File(urlArchivo);
    }
    
    public void AbrirFlujo() throws IOException{
        if(!archivo.exists()){
            archivo.createNewFile();
            flujo = new RandomAccessFile(archivo, "rw");
            flujo.seek(0);
            flujo.writeInt(0); //n  (cant de registros)
            flujo.writeInt(0); //k  (cant de id)
        }else{
            flujo = new RandomAccessFile(archivo, "rw");
        }
    }
    
    public void CerrarFlujo() throws IOException{
        if(flujo != null){
            flujo.close();
        }
    }
    
    
    
    @Override
    public void Guardar(Sucursal t) {
        try {
            AbrirFlujo();
            flujo.seek(0);
            int n = flujo.readInt();
            int k = flujo.readInt();
            
            long pos = 8 + (n * TAMAÑO);
            flujo.seek(pos);
            
            flujo.writeInt(++k); //reemplaza el identificador de la sucursal
            flujo.writeUTF(urlArchivo);
            
            
            
            
        } catch (IOException ex) {
            Logger.getLogger(SucursalImplement.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void Modificar(Sucursal t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean Eliminar(Sucursal t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Sucursal[] mostrarTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
    
}
