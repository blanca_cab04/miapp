/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.uni.edu.programacion1.dao;

/**
 *
 * @author Blanca Payan
 */
public interface SucursalInterface<T> {
    public void Guardar(T t);
    public void Modificar(T t);
    public boolean Eliminar(T t);
    public T[] mostrarTodos();
}
